const router = require("express").Router();

const SessionController = require("../Controllers/Session.controller");

router.post("/verificar", SessionController.verificar);

module.exports = router;
