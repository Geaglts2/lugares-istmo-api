const { app, server } = require("./server");

app.listen(app.get("port"), () => {
  console.log(
    `El servidor esta corriendo en http://localhost:${app.get("port")}${
      server.graphqlPath
    }`
  );
});
