require("dotenv").config();
const { verify } = require("jsonwebtoken");

module.exports = (context) => {
  const Authorization = context.req.get("Authorization");
  if (Authorization) {
    const token = Authorization.replace("Bearer ", "");
    const { usuario_id } = verify(token, process.env.TOKEN_KEY);
    return usuario_id;
  }

  return null;
};
