module.exports = (str) => {
  let new_str = str.toLowerCase().trim();
  return new_str.replace(/\b\w/g, (c) => c.toUpperCase());
};
