const { compare } = require("bcrypt");

module.exports = async (contrasena_ingresada, contrasena) => {
  return await compare(contrasena_ingresada, contrasena);
};
