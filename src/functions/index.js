const CapitalizeWords = require("./CapitalizeWors");
const EncryptPassword = require("./EncryptPassword");
const ComparePassword = require("./ComparePassword");
const GenerateToken = require("./GenerateToken");
const GetUserId = require("./GetUserId");
const IsAdmin = require("./IsAdmin");

module.exports = {
  CapitalizeWords,
  EncryptPassword,
  ComparePassword,
  GenerateToken,
  GetUserId,
  IsAdmin,
};
