if (process.env.NODE_ENV !== "production") {
  require("dotenv").config();
}
const { sign } = require("jsonwebtoken");

module.exports = (data) => {
  return sign(data, process.env.TOKEN_KEY);
};
