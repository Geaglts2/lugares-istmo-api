require("dotenv").config();
const { verify } = require("jsonwebtoken");

module.exports = async (context) => {
  const Authorization = context.req.headers.authorization;
  if (Authorization) {
    const token = Authorization.replace("Bearer ", "");
    const { usuario_id } = verify(token, process.env.TOKEN_KEY);

    const usuario = await context.prisma.usuarios.findOne({
      where: {
        id: usuario_id,
      },
    });

    return usuario.admin;
  }

  return false;
};
