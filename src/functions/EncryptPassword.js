const { hash, genSalt } = require("bcrypt");

module.exports = async (password) => {
  return await hash(password, await genSalt(10));
};
