const { resenas } = require("./Query");

module.exports = {
    Lugar: {
        async usuario(parent, data, context) {
            return await context.prisma.usuarios.findOne({
                where: {
                    id: parent.usuario_id,
                },
            });
        },
        async resenas(parent, data, context) {
            return await context.prisma.resenas.findMany({
                where: {
                    lugar_id: parent.id,
                },
            });
        },
    },
    Usuario: {
        async lugares(parent, data, context) {
            return await context.prisma.lugares.findMany({
                where: {
                    usuario_id: parent.id,
                },
            });
        },
        async resenas(parent, data, context) {
            return await context.prisma.resenas.findMany({
                where: {
                    usuario_id: parent.id,
                },
            });
        },
        async roles(parent, data, context) {
            const roles = await context.prisma.rol_usuario.findMany({
                where: {
                    usuario_id: parent.id,
                },
                include: {
                    rol: true,
                },
            });

            return roles.map((item) => item.rol);
        },
    },
    Resena: {
        async usuario(parent, data, context) {
            return await context.prisma.usuarios.findOne({
                where: {
                    id: parent.usuario_id,
                },
            });
        },
        async lugar(parent, data, context) {
            return await context.prisma.lugares.findOne({
                where: {
                    id: parent.lugar_id,
                },
            });
        },
    },
};
