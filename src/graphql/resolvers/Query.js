const { IsAdmin, GetUserId } = require("../../functions");

module.exports = {
    info() {
        return `Se encuentra en el servidor de lugares istmo`;
    },
    async roles(parent, data, { prisma }) {
        return await prisma.roles.findMany();
    },
    async usuarios(parent, data, context) {
        const isAdmin = await IsAdmin(context);
        if (!isAdmin) return [];

        return await context.prisma.usuarios.findMany();
    },
    async lugares(parent, data, context) {
        return await context.prisma.lugares.findMany();
    },
    async resenas(parent, data, context) {
        return await context.prisma.resenas.findMany();
    },
    async me(parent, data, context) {
        const userId = GetUserId(context);
        return await context.prisma.usuarios.findOne({
            where: {
                id: userId,
            },
        });
    },
};
