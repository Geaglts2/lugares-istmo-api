const {
    CapitalizeWords,
    EncryptPassword,
    ComparePassword,
    GenerateToken,
    GetUserId,
} = require("../../functions/");

module.exports = {
    async registro(parent, args, { prisma }) {
        try {
            const countUsers = await prisma.usuarios.count();

            const {
                nombre,
                apaterno,
                amaterno,
                usuario,
                correo,
            } = args.informacion;

            let fields = {
                ...args.informacion,
                nombre,
                apaterno,
                amaterno,
                usuario,
                correo: correo.toLowerCase().trim(),
                admin: countUsers > 0 ? false : true,
            };

            const userExists = await prisma.usuarios.findMany({
                where: {
                    OR: [
                        {
                            usuario: {
                                equals: fields.usuario,
                            },
                        },
                        {
                            correo: fields.correo,
                        },
                    ],
                },
            });

            if (userExists.length > 0) {
                return false;
            }

            fields = {
                ...fields,
                contrasena: await EncryptPassword(fields.contrasena),
            };

            await prisma.usuarios.create({
                data: {
                    ...fields,
                    roles: {
                        create: {
                            rol: {
                                connect: {
                                    id: 1,
                                },
                            },
                        },
                    },
                },
            });

            return true;
        } catch (e) {
            console.log(e);
            return false;
        }
    },
    async login(parent, args, { prisma }) {
        const { correo, usuario, contrasena } = args;

        let userExists = {};

        if (correo) {
            userExists = await prisma.usuarios.findOne({
                where: {
                    correo,
                },
            });
        } else if (usuario) {
            userExists = await prisma.usuarios.findOne({
                where: {
                    usuario,
                },
            });
        } else {
            return "Usuario y/o Contraseña incorrecta";
        }

        if (!userExists) return "Usuario y/o Contraseña incorrecta";

        const matchPassword = await ComparePassword(
            contrasena,
            userExists.contrasena
        );

        if (!matchPassword) return "Usuario y/o Contraseña incorrecta";

        const roles = await prisma.roles_usuario.findMany({
            where: {
                usuario_id: userExists.id,
            },
            select: {
                rol: {
                    select: {
                        rol: true,
                    },
                },
            },
        });

        const generatedData = {
            id: userExists.id,
            admin: userExists.admin,
            correo: userExists.correo,
            usuario: userExists.usuario,
            roles: roles.map(({ rol }) => rol.rol),
        };

        const token = GenerateToken(generatedData);

        await prisma.usuarios.update({
            where: { id: userExists.id },
            data: {
                token,
            },
        });

        return { token, admin: generatedData.admin };
    },
    async nuevoLugar(parent, { campos }, { user, prisma }) {
        try {
            if (!user) return false;

            const newPlace = await prisma.lugares.create({
                data: {
                    ...campos,
                    usuario: {
                        connect: { id: user.id },
                    },
                },
            });

            return newPlace;
        } catch (e) {
            return null;
        }
    },
};
