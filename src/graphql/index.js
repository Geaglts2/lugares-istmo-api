const { join } = require("path");
const { PrismaClient } = require("@prisma/client");

const { ApolloServer } = require("apollo-server-express");
const {
    loadSchemaSync,
    GraphQLFileLoader,
    addResolversToSchema,
} = require("graphql-tools");

// importa el schema pero con el archivo de extencion, graphql
const schema = loadSchemaSync(join(__dirname, "schema.graphql"), {
    loaders: [new GraphQLFileLoader()],
});

const resolvers = require("./resolvers");
const { verify } = require("jsonwebtoken");

// Union de los schemas con los resolvers
const schemaWithResolvers = addResolversToSchema({
    schema,
    resolvers,
});

module.exports = new ApolloServer({
    schema: schemaWithResolvers,
    context: (req) => {
        let user = null;
        const Authorization = req.req.get("Authorization");

        if (Authorization) {
            const token = Authorization.replace("Bearer ", "");
            user = verify(token, process.env.TOKEN_KEY);
        }

        return {
            ...req,
            prisma: new PrismaClient(),
            user,
        };
    },
    playground: true,
});
