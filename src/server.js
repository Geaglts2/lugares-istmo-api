require("dotenv").config();
const express = require("express");
const graphqlServer = require("./graphql");

const app = express();

app.set("port", process.env.PORT);

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// routes
const { SessionRoutes } = require("./Routes");

app.use("/sesion", SessionRoutes);

graphqlServer.applyMiddleware({ app });

app.use("*", (req, res) => {
  res.json({ codigo: "404" });
});

module.exports = { app, server: graphqlServer };
